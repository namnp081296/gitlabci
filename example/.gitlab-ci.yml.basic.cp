stages:
    - dev
    - sandbox
    - staging
    - production

dev_a:
  stage: dev
  script:
    - echo "This is the first job of dev"
  environment: dev
  resource_group: dev
  #tags:
  #  - runnera

dev_b:
  stage: dev
  script:
    - echo "This is the second job of dev"
  environment: dev
  resource_group: dev
  #tags:
  #  - runnerb

dev_c:
  stage: dev
  script:
    - echo "This is the first job of dev"
  environment: dev
  resource_group: dev
  #tags:
  #  - runnerc

sandbox_a:
  stage: sandbox
  script:
    - echo "This is the first job of sandbox stage"
  environment: sandbox
  resource_group: sandbox
  needs: [dev_a]
  #tags:
  #  - runnera

sandbox_b:
  stage: sandbox
  script:
    - echo "This is second job"
  environment: sandbox
  resource_group: sandbox
  needs: [dev_b]
  #tags:
  #  - runnerb

sandbox_c:
  stage: sandbox
  script:
    - echo "This is the first job of sandbox stage"
  environment: sandbox
  resource_group: sandbox
  needs: [dev_c]
  #tags:
  #  - runnerc

staging_a:
  stage: staging
  script:
    - echo "Ready to run first job in staging stage"
  environment: staging
  resource_group: staging
  needs: [sandbox_a]
  #tags:
  #  - runnera

staging_b:
  stage: staging
  script:
    - echo "This is the second jobs. It takes more time to finish"
  environment: staging
  resource_group: staging
  needs: [sandbox_b]
  #tags:
  #  - runnerb

staging_c:
  stage: staging
  script:
    - echo "Ready to run first job in staging stage"
  environment: staging
  resource_group: staging
  needs: [sandbox_c]
  #tags:
  #  - runnerc

production_a:
  stage: production
  script:
    - echo "Ready for publishing to Production"
    - echo "We're checking it first"
  environment: production
  resource_group: production
  needs: [staging_a]
  #tags:
  #  - runnera

production_b:
  stage: production
  script:
    - echo "Everything gonna be OK! It will finish soon"
  environment: production
  resource_group: production
  needs: [staging_b]
  #tags:
  #  - runnerb

production_c:
  stage: production
  script:
    - echo "Ready for publishing to Production"
    - echo "We're checking it first"
  environment: production
  resource_group: production
  needs: [staging_c]
  #tags:
  #  - runnerc

