stages:
  - test
  - deploy
  - staging
  - production

cache:
 paths:
    - node_modules/

test:
  #image: node:latest
  stage: test
  tags:
    #- docker
    - runnerb
  script:
    - npm install
    - npm run test
  cache:
    key: test-cache
  #artifacts:  
    paths:
      - node_modules/
      #- test/
      #- staging/

deploy:
  stage: deploy
  tags:
    - runnerb
  script:
    - echo "Ready to build again"
    - cp -r -u * $PROJECT_DIR
    - cd $PROJECT_DIR
    - npm install
    - forever stop index.js || true
    - forever start index.js
  #dependencies:
  #  - test
  #cache:
    #key: build-cache
  #artifacts:  
    #paths:
      #- node_modules/
      #- deploy/
      #- test/

staging:
  stage: staging
  tags:
    - runnerb
  script:
    - echo "Ready to build"
    - npm install
    - npm run test
    - echo "Build done"
  #dependencies:
  #  - test
  #cache:    
  #artifacts:
    #key: test-cache
    #policy: pull
    #paths: 
      #- node_modules/
      #- staging/
      #- deploy/

#before_release: 
#  stage: staging
#  tags:
#    - runnerb
#  script:
#    - echo "This is check how artifacts works"
#    - npm install
#    - npm run test 
#    - echo "Done. Ready for last stage!"
  #cache:
    #key: build-cache
    #paths:
      #- node_modules/
    #policy: pull

production:
  stage: production
  tags: 
    - runnerb
  script:
    - echo "Last build for production"
    - npm install
    - npm run test
    - echo "Done."
  #dependencies:
  #  - deploy
  #artifacts: 
  #  paths: 
  #    - node_modules/
  #    - test/
  #    - deploy/ 
