stages:
    - dev
    - sandbox
    - staging
    - production

dev_a:
  stage: dev
  script:
    - echo "This is the first job of dev"

dev_b:
  stage: dev
  script:
    - echo "This is the second job of dev"

dev_c:
  stage: dev
  script:
    - echo "This is the last job of dev. It will finish soon!"
    - echo "Last job is complete. Ready for the next stage"

sandbox_a:
  stage: sandbox
  script:
    - echo "This is the first job of sandbox stage"
  needs: [dev_a]

sandbox_b:
  stage: sandbox
  script:
    - echo "This is second job" 
  needs: [dev_b]

sandbox_c:
  stage: sandbox
  script:
    - echo "This is the last job ob sanbox stage" 
  needs: [dev_c]

staging_a:
  stage: staging
  script:
    - echo "Ready to run first job in staging stage"
  needs: [sandbox_a]

staging_b:
  stage: staging
  script:
    - echo "This is the second jobs. It takes more time to finish"
  needs: [sandbox_b]

staging_c:
  stage: staging
  script:
    - echo "Last job is complete! Ready for the last stage"
  needs: [sandbox_c]

production_a:
  stage: production
  script:
    - echo "Ready for publishing to Production"
    - echo "We're checking it first"
  needs: [staging_a]

production_b:
  stage: production
  script:
    - echo "Everything gonna be OK! It will finish soon"
  needs: [staging_b]

production_c:
  stage: production
  script:
    - echo "Code check is complete. Ready for Publishing"
    - echo "Everything has done!"
  needs: [staging_c]
