stages:
    - dev
    - staging
    - production

.default_script: &default_script
  - 'which ssh-agent || ( apk add openssh-client)'
  - eval "$(ssh-agent -s)"
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh

job_dev:
  stage: dev
  script:
    - echo "Ready for building code"
    - python employee.py
    - echo "Ahihi! Run code done! hihi"
  #when: on_success
  allow_failure: true
  artifacts:
    paths:
      - employee.py
  tags:
    - runnera
  only:
    #- sandbox
    - merge_requests 
    #- master

job_staging:
  stage: staging
  script:
    - *default_script
    - echo "$SSH_PRIVATE_KEY_STAGING" | tr -d '\r' | ssh-add - > /dev/null
    - echo "Ready for deploying code to Staging server"
    - ssh -o "StrictHostKeyChecking=no" -p22 $TARGET_STAGING "mkdir -p /home/deployment/test_app/app/"
    - scp employee.py $TARGET_STAGING:/home/deployment/test_app/app/
    - ssh -o "StrictHostKeyChecking=no" -p22 $TARGET_STAGING "cd /home/deployment/test_app/app/ && python employee.py"
    - echo "Running script done hihi!"
  #when: on_success
  tags:
    - runnerb
  only:
    #- sandbox
    - merge_requests
    #- master

job_production:
  stage: production
  script:
    - *default_script
    - echo "$SSH_PRIVATE_KEY_PRODUCTION" | tr -d '\r' | ssh-add - > /dev/null
    - echo "Ready for deploying code to Production server"
    - ssh -o "StrictHostKeyChecking=no" -p22 $TARGET_PRODUCTION "mkdir -p /home/production/zserver/"
    - scp employee.py $TARGET_PRODUCTION:/home/production/zserver/
    - ssh -o "StrictHostKeyChecking=no" -p22 $TARGET_PRODUCTION "cd /home/production/zserver/ && python employee.py"
    - echo "Running script done! hihi"
  tags:
    - runnerc
  only:
    #- sandbox
    - merge_requests
    #- master
