stages:
  - test
  - deploy

cache:
  paths:
    - node_modules/

test:
  image: node:latest
  stage: test
  tags:
    - docker
  script:
    - npm install
    - npm run test
  cache:
    key: build-cache
    paths:
      - node_modules/

deploy:
  stage: deploy
  tags:
    - runnera
  script:
    - echo "Ready to build" 
    - cp -r -u * $PROJECT_DIR
    - cd $PROJECT_DIR
    - npm install
    - forever stop index.js || true
    - forever start index.js
  cache:
    key: build-cache
    paths:
      - node_modules/
