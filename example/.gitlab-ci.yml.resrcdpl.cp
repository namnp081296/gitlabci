deploy_job1:
  stage: deploy
  script:
    - echo "Job 1 This is for test Resource Group"
  environment: staging
  resource_group: staging
  tags:
    - runnera

deploy_job2:
  stage: deploy
  script:
    - echo "Job 2 This is for test Resource Group"
  environment: staging
  resource_group: staging
  tags:
    - runnerb

deploy_job3:
  stage: deploy
  script:
    - echo "Job 3 This is for test Resource Group"
  environment: production
  resource_group: production
  tags:
    - runnera

deploy_job4:
  stage: deploy
  script:
    - echo "Job 4 This is for test Resource Group"
  environment: production
  resource_group: production
  tags:
    - runnerb




#deploy_review:
#  stage: deploy
#  script:
#    - echo "Deploy a review app"
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    url: https://$CI_ENVIRONMENT_SLUG.example.com
#    on_stop: stop_review
#  rules:
#    - if: $CI_MERGE_REQUEST_ID

#stop_review:
#  stage: deploy
#  script:
#    - echo "Remove review app"
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    action: stop
#  rules:
#    - if: $CI_MERGE_REQUEST_ID
#      when: manual
