workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID

review:
  script:
    - sleep 5s
    - echo "Deploy Review Apps"
    - DYNAMIC_ENV_URL="https://www.gitlab-${CI_MERGE_REQUEST_ID}.com"
    - echo "DYNAMIC_ENV_URL=$DYNAMIC_ENV_URL"
  artifacts:
    reports:
      dotenv: deploy.env
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: $DYNAMIC_ENV_URL
    on_stop: stop_review

stop_review:
  script:
    - echo "Stop Review App"
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
