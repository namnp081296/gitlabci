stages:
  - test
  - staging

variables:
  TEST_VAR: "All jobs can use this variable's value"

job1:
  stage: test
  variables:
    TEST_VAR_JOB: "Only job1 can use this variable's value"
  script:
    - echo "$TEST_VAR" and "$TEST_VAR_JOB"
    - echo "We are in the $CI_JOB_STAGE stage"
    # Master Branch echo Protected Variables
    - echo "This is Protected $TARGET_PRODUCTION variables"
  tags:
    - runnera

job2:
  stage: test
  script:
    - export
  needs: [job1]
  tags:
    - runnerb

job3 staging 1/3:
  stage: staging
  script:
    - echo "Staging 1/3"
  needs: [job1]
  tags:
    - runnera

job3 staging 2/3:
  stage: staging
  script: 
    - echo "Staging 2/3"
  needs: [job1]
  tags:
    - runnerb

job3 staging 3/3:
  stage: staging
  script:
    - echo "Staging 3/3"
  needs: [job1]
  tags:
    - runnerc
