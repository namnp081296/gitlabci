#!/usr/bin/python

class Employee:
   'Common base class for all employees'
   empCount = 0
  
 
   def __init__(self, name, salary):
      self.name = name
      self.salary = salary
      Employee.empCount += 1
    
   def displayCount(self):
     print "Total Employee %d" % Employee.empCount

   def displayEmployee(self):
     print "Name: ", self.name, "Salary :", self.salary

"Create first object of Employee class"
emp1 = Employee("Zara", 2000)
"The second object of Employee class"
emp2 = Employee("Manni", 5000)
"The thirth object of Employee class"
emp3 = Employee("Jane",3500)
"Fourth Employee"
emp4 = Employee("Kane, 4200")
"Display Employee"
emp1.displayEmployee()
emp2.displayEmployee()
emp3.displayEmployee()
emp4.displayEmployee()
print "Total Employee %d" % Employee.empCount
