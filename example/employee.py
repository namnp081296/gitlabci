class Employee:
   'Common base class for all employees'
   empCount = 0
   def __init__(self, name, salary, age, job, mail):
      self.name = name
      self.salary = salary
      self.age = age
      self.job = job
      self.mail = mail
      Employee.empCount += 1
   def displayCount(self):
     print ("Total Employee %d" % Employee.empCount)

   def displayEmployee(self):
     print ("Name:", self.name, "\nSalary:", self.salary, "\nAge:", self.age, "\nJob:",self.job, "\nMail:",self.mail)
     
if __name__ == '__main__':
    "The first & second object of Employee class"
    emp1 = Employee("Jenny", 4000, 20, "Secretary", 'jenny@gmail.com')
    emp2 = Employee("Justin", 2320, 25, "IT Engineer", 'justin-john.0196@education.com')
    "Display Employee"
    emp1.displayEmployee()
    emp2.displayEmployee()
    #print ("%d" % Employee.empCount)

    # Dev additional
    "The third and fourth Employee"
    emp3= Employee("John",4000,24,"Taxi Driver","johnakataxidrv@gmail.com")
    emp4= Employee("Kelly",3500,23,"Telesale","kellypon@outlook.com")
    "Display third and fourth employee"
    emp3.displayEmployee()
    emp4.displayEmployee()

    # Sandbox Trigger 13 Comment 5 Employee and comment 6 employee
    emp5 = Employee("Kaneuniae", 2300, 32, "Electric", "kane@electric.com") 
    emp6 = Employee("Billy Jaycez", 2123, 20, "Sale", "billy@salewerr.com")
    emp5.displayEmployee()
    emp6.displayEmployee()

    # Sandbox Trigger 11 unComment 7 Employee and comment 8 Employee
    emp7 = Employee("Daniel", 3500, 32, "Bus Driver", "Dani@budri.com")
    emp8 = Employee("Kayz", 2700, 28, "Superstart", "kayz@ss.com")
    emp7.displayEmployee()
    emp8.displayEmployee()
    

    # Sanbox Trigger 17.1
    #emp9 = Employee("Daniel Kim", 5500, 31, "tax reporter", "DanielK@tax.com")
    #emp9.displayEmployee()
    # Sandbox Trigger 18.1
    #emp10 = Employee("Kayz Nuz", 2250, 21, "Gymer", "kainyz@gym.com")
    #emp10.displayEmployee()
    
    print ("%d" % Employee.empCount)
