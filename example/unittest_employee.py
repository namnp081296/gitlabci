import unittest
from employee import Employee

class TestEmployee(unittest.TestCase):    
    def test_name(self):
        usr1 = Employee("Jenny", 4000, 20, 'Secretary', 'jenny@gmail.com')
        usr2 = Employee("Justin", 2320, 25, 'IT Engineer', 'justin-john.0196@education.com')
        usr3 = Employee("Billy", 3620, 34, 'Director', 'akabilly@aws-us.net')
        self.assertEqual(usr1.name, "Jenny")
        self.assertNotEqual(usr2.name, 'Kane')
        
    def test_salary(self):
        usr1 = Employee("Jenny", 4000, 20, 'Secretary', 'jenny@gmail.com')
        usr2 = Employee("Justin", 2320, 25, 'IT Engineer', 'justin-john.0196@education.com')
        usr3 = Employee("Billy", 3620, 34, 'Director', 'akabilly@aws-us.net')
        self.assertEqual(usr3.salary, 3620)
        self.assertGreater(usr1.salary, 2000)
        
    def test_age(self):
        usr1 = Employee("Jenny", 4000, 20, 'Secretary', 'jenny@gmail.com')
        usr2 = Employee("Justin", 2320, 25, 'IT Engineer', 'justin-john.0196@education.com')
        usr3 = Employee("Billy", 3620, 34, 'Director', 'akabilly@aws-us.net')
        self.assertEqual(usr2.age, 25)
        self.assertGreater(usr3.age, 21)
        
    def test_job(self):
        usr1 = Employee("Jenny", 4000, 20, 'Secretary', 'jenny@gmail.com')
        usr2 = Employee("Justin", 2320, 25, 'IT Engineer', 'justin-john.0196@education.com')
        usr3 = Employee("Billy", 3620, 34, 'Director', 'akabilly@aws-us.net')
        self.assertEqual(usr1.job, 'Secretary')
        self.assertNotEqual(usr2.job, 'Chief')
    
    def test_mail(self):
        usr1 = Employee("Jenny", 4000, 20, 'Secretary', 'jenny@gmail.com')
        usr2 = Employee("Justin", 2320, 25, 'IT Engineer', 'justin-john.0196@education.com')
        usr3 = Employee("Billy", 3620, 34, 'Director', 'akabilly@aws-us.net')
        mail_length_usr3 = len(usr3.mail)
        self.assertGreater(mail_length_usr3, 10)
        

if __name__ == '__main__':
    unittest.main()
